# -*- coding: utf-8 -*-

import os, time
import requests, zipfile
import geopandas as gp
import pandas as pd
import re
# from unidecode import unidecode
import pymysql
from sqlalchemy import create_engine

class Mapas:
    def __init__(self, year:int=None,test:bool=False,**kwargs):

        self._year = year
        self._verbose: int = 0
        self._test = test
        if kwargs:
            self._kwargs = kwargs
        else:
            self._kwargs = {}
        self._geojson_options = {}
        self._output_dfs = []
        self.setup()
        self.aditional_setup()
        
    def set_verbosity(self,verbosity: int):
        self._verbose = verbosity
    
    def show_msg(self,msg:str,min_verbosity:int):
        if self._verbose >= min_verbosity:
            print(msg)
    
    def setup(self):
        if self._test:
            self._data_dir = '../test/ref_csv/'
        else:
            self._data_dir = '../dados/'
        
        self._data_dir += self.get_name()
        self._input_folder = f'{self._data_dir}/input'
        self._output_folder = f'{self._data_dir}/output'
        
        input_files = self.get_input_files()
        self._input_paths = [ f'{self._input_folder}/{file}' for file in  input_files]
        
        output_files = self.get_output_files()
        self._output_paths = [ f'{self._output_folder}/{file}' for file in  output_files]

        self._sql_tables = [file.split('.')[0].upper() for file in output_files]
        
           
    def aditional_setup(self): # may be be overridden by subclass
        pass

    def get_name(self): # must be overridden by subclass
        msg = 'O método get_name() deve ser definido nas classes derivadas.'
        raise Exception(msg)
    
    def get_input_files(self): # must be overridden by subclass
        msg = 'O método get_input_files() deve ser definido nas classes derivadas.'
        raise Exception(msg)
    
    def get_output_files(self): # must be overridden by subclass
        msg = 'O método get_output_files() deve ser definido nas classes derivadas.'
        raise Exception(msg)

    def set_verbose(self,option: bool):
        self._verbose = option
        
    # def etl(self, update: bool=False ):
    #     self.extract(update)
    #     self.transform(update)
    #     self.load(update)

    def extract(self, update: bool=False):

        if self._verbose:
            print('Iniciando extração dos dados.')
    
        start =time.time()
        os.makedirs(self._input_folder, exist_ok=True)
        update_extract = False
        if not update:
            for file_path in self._input_paths:
                if not os.path.isfile(file_path):
                    update_extract = True
                    msg = f'Arquivo {file_path} não encontrado. Todos arquivos de entrada serão atualizados.'
                    break
        else:
            update_extract = True
            if len(self._input_paths) > 1:
                msg = f'Os arquivos de entrada serão atualizados.'
            else:
                msg = f'O arquivo de entrada será atualizado.'
            
        if update_extract:
            self.setup_extract()
            self.save_metadata()
        else:
            if len(self._input_paths) > 1:
                msg = f'Os arquivos de entrada já existiam e foram mantidos. Para atualizá-los, utilize update=True.'
            else:
                msg = f'O arquivo de entrada já existia e foi mantido. Para atualizá-lo, utilize update=True.'

        delta = time.time() - start
        if self._verbose:
            sufixo = 's' if len(self._input_paths) > 1 else ''
            if self._input_paths:
                msg += f'\n Arquivo{sufixo} de entrada:\n'
                for path in self._input_paths:
                    msg += f'   > {path}.\n'
            msg += f"\n----- {'Tempo total de extração:':<10s} {delta:.1f} segundos. -----\n"
            print(msg)

    def setup_extract(self):
        err_msg = 'O método setup_extract() deve ser definido nas classes derivadas.'
        raise Exception(err_msg)
    
    def transform(self,update: bool=False):

        if self._verbose:
            print('Iniciando processamento dos dados.')

        start = time.time()
        os.makedirs(self._output_folder, exist_ok=True)

        update_transform = False
        msg = '\n'
        if not update:
            for file_path in self._output_paths:
                if not os.path.isfile(file_path):
                    update_transform = True
                    msg += f'Arquivo {file_path} não encontrado. Todos arquivos de saída serão atualizados.'
                    break
        else:
            update_transform = True
            
        if update_transform:
            self.setup_transform()
            # self.save_metadata()
            
            for csv_file, df in zip(self._output_paths,self._output_dfs):
                if os.path.isfile(csv_file):
                    old_csv = csv_file.replace('.csv','_antigo.csv')
                    msg += f'O arquivo {csv_file} foi atualizado.\n'
                    os.rename(csv_file,old_csv)
                    df.to_csv(csv_file,index=False)
                else:
                    df.to_csv(csv_file,index=False)
                    msg += f'O arquivo {csv_file} foi salvo.\n'
        else:
            self._output_dfs = []
            for csv_file in self._output_paths:
                df = pd.read_csv(csv_file)
                self._output_dfs.append(df)

            if len(self._output_paths) > 1:
                msg = 'Os arquivos de saída já existiam e foram mantidos. Para atualizá-los, utilize update=True.\n'
            else:
                msg = 'O arquivo de saída já existia e foi mantido. Para atualizá-lo, utilize update=True.\n'
       
        delta = time.time() - start
        if self._verbose:
            sufixo = 's' if len(self._output_paths) > 1 else ''
            if self._output_paths:
                msg += f'\n Arquivo{sufixo} de saída:\n'
                for path in self._output_paths:
                    msg += f'   > {path}.\n'
            msg += f"\n----- {'Tempo total de processamento:':<10s} {delta:.1f} segundos. -----\n"
            print(msg)

        return

    def setup_transform(self):
        err_msg = 'O método setup_transform() deve ser definido nas classes derivadas.'
        raise Exception(err_msg)

    def round_float(self,input_str:str,fmt_str:str='4g'):

        assert fmt_str[-1] in ['f','g']

        pattern = r"[-+]?\d*\.\d+"
        float_numbers = re.findall(pattern, input_str)
        rounded_floats = [f'{float(x):.{fmt_str}}' for x in float_numbers]

        pat=re.compile(pattern)
        sep = '$$$'
        assert sep not in input_str
        new_str_lines = pat.sub(sep, input_str).split(sep)

        output_str = ''
        for x,y in zip(new_str_lines[:-1],rounded_floats):
            output_str += x + y
        output_str += new_str_lines[-1]

        return output_str
   
    def get_db_connection_parameters(self):
        
        db_conn_path = 'db_connection/db_connection.json'
        
        if not os.path.isfile(db_conn_path):
            # rodando teste, procurar um nível acima
            if os.path.isfile(f'../{db_conn_path}'):
                db_conn_path = f'../{db_conn_path}'
            
        if os.path.isfile(db_conn_path):
            with open(db_conn_path,'r') as f:
                content = f.read()
            return eval(content)
        
        else:
            base_file = db_conn_path.split('.')[0] + '__base.json'
            print(f"Arquivo '{db_conn_path}' não encontrado.\nUtilizar como referência o arquivo '{base_file}', incluindo user e passwd.")

    def setup_load(self, sql_table_columns_names: list=[]):
        raise Exception('Deve ser definida nas classes derivadas.')
    
    def get_load_command(self,sql_table:str,csv_file:str):
        '''
        retorna uma string com comando SQL para carregar tabela
        a partir de arquivo csv.
        '''
        
        sql_command = f"""
        LOAD DATA LOCAL INFILE '{csv_file}' 
        INTO TABLE `{sql_table}` 
        CHARACTER SET UTF8 
        FIELDS TERMINATED BY ',' 
        ENCLOSED BY '"'
        IGNORE 1 LINES;
        """
        return sql_command
    
    def load(self, local_infile = False ):
        
        start = time.time()
        
        db_connection = self.get_db_connection_parameters()
        if not db_connection:
            return

        if self._verbose:
            self.show_msg(f"\n Carregando tabelas... - Host: {db_connection['host']} - DB: {db_connection['db']}\n",min_verbosity=1)

        try:
            # Create MySQL Connection
            connection = pymysql.connect(host=db_connection['host'],
                                         user=db_connection['user'],
                                         password=db_connection['passwd'],
                                         db=db_connection['db'],
                                         local_infile=local_infile)
            cursor = connection.cursor()

            user=db_connection['user']
            pw=db_connection['passwd']
            host=db_connection['host']
            db=db_connection['db']
            engine = create_engine(f"mysql+pymysql://{user}:{pw}@{host}/{db}")

            if local_infile:
                # make sure database accepts local file load
                cursor.execute("SET @@global.local_infile = 1;")
                for csv_file, sql_table in zip(self._output_paths,self._sql_tables):
                    if self._verbose >= 1:
                        print(f'> Tabela: {sql_table}...', end = '')

                    if not os.path.exists(csv_file):
                        self.show_msg(f'arquivo não encontrado: {csv_file}',min_verbosity=1)
                        break

                    # comandos CREATE TABLE, ALTER TABLE `{sql_table}` ADD KEY, etc.
                    sql_commands = self.setup_load(csv_file,sql_table)
                    for sql_command in sql_commands:
                        self.show_msg(f'\n >> {time.time() - start:.2f} - {sql_command}\n',min_verbosity=2)
                        cursor.execute(sql_command)
                    
                    if self._verbose >= 1:
                        print(' OK!')

            else:
                for df, sql_table in zip(self._output_dfs,self._sql_tables):
                    if self._verbose >= 1:
                        print(f'> Tabela: {sql_table}...', end = '')
                    
                    df.to_sql(sql_table, con = engine, index=False, if_exists = 'append',method='multi')
                    
                    if self._verbose >= 1:
                        print(' OK!')
            
        except OSError as e:
            self.show_msg(e,min_verbosity=0)
        
        finally:
            connection.commit()
            connection.close()
        
        delta = time.time() - start
        self.show_msg(f"\n----- {'Load:':<10s} {delta:.1f} segundos. -----",min_verbosity=1)

    def drop_table(self, sql_table: str):
        query = f"DROP TABLE IF EXISTS {sql_table};"
        self.mysql_query(query)
        return
    
    def describe_table(self, sql_table: str):
        query = f'DESCRIBE {sql_table};'
        lines = self.mysql_query(query)
        columns = ['Field','Type','Null','Key','Default','Extra']
        df = pd.DataFrame(lines,columns=columns)
        return df
    
    def get_table_memory(self, sql_table: str=''):
        db_connection = self.get_db_connection_parameters()
        query = f""" 
        SELECT TABLE_NAME AS `Table`, ROUND((DATA_LENGTH + INDEX_LENGTH) / 1024 / 1024) AS `Size (MB)`
        FROM information_schema.TABLES
        WHERE TABLE_SCHEMA = '{db_connection['db']}'  """
        if sql_table:
            query += f" AND  TABLE_NAME = '{sql_table}' ;"
        else:
            query += " ORDER BY  (DATA_LENGTH + INDEX_LENGTH)  DESC;"
        lines = self.mysql_query(query)
        columns = ['Tabela','Memória (MB)']
        df = pd.DataFrame(lines,columns=columns)
        return df
    
    def mysql_query(self, query:str):
               
        db_connection = self.get_db_connection_parameters()
        if not db_connection:
            return

        try:
            # Create MySQL Connection
            connection = pymysql.connect(host=db_connection['host'],
                                         user=db_connection['user'],
                                         password=db_connection['passwd'],
                                         db=db_connection['db'])
            cursor = connection.cursor()
            
        except OSError as e:
            print(e)
        
        else:
            if self._verbose:
                print(f'Query: {query}')
            cursor.execute(query)
            table_lines = cursor.fetchall()
        
        finally:
            connection.commit()
            connection.close()
        
        return table_lines

    
    def url2zip(self, url, filename):
        if self._verbose:
            print(f'iniciando download de {url}...')

        r = requests.get(url)
        # Retrieve HTTP meta-data
        if r.status_code == 200:
            msg = 'Download realizado com sucesso.'
        elif r.status_code == 404:
            raise ValueError(f'url não encontrada: {url}')
        else:
            print(f'O download apresentou problemas. Código: {r.status_code}')
            msg = ''

        msg += f'escrevendo conteudo no arquivo <{filename}>.'

        with open(filename, 'wb') as f:
            f.write(r.content)
        if self._verbose:
            print(msg)
        
        return

    def extractFromZip(self, zip_file, file_name_tail=None):
        # subfolder = zip_file.split('/')[-1].rstrip('.zip')
        out_dir = zip_file.rstrip('.zip')
        if self._verbose:
            print(f'Extraindo arquivo para a pasta <{out_dir}> ... ', end='')
        # directory_to_extract_to = subfolder
        with zipfile.ZipFile(zip_file, 'r') as zipObj:
            if file_name_tail:
                for file_name in zipObj.namelist():
                    if file_name.endswith(file_name_tail):
                        zipObj.extract(file_name, out_dir)
            else:
                zipObj.extractall(out_dir)
        if self._verbose:
            print('OK!')

        return

    def get_sigla_uf_dict(self):
        try:
            self._sigla_uf_dict
        except:
            self._sigla_uf_dict = {
                'AC': 'Acre',
                'AL': 'Alagoas',
                'AP': 'Amapá',
                'AM': 'Amazonas',
                'BA': 'Bahia',
                'CE': 'Ceará',
                'DF': 'Distrito Federal',
                'ES': 'Espírito Santo',
                'GO': 'Goiás',
                'MA': 'Maranhão',
                'MG': 'Minas Gerais',
                'MS': 'Mato Grosso do Sul',
                'MT': 'Mato Grosso',
                'PA': 'Pará',
                'PB': 'Paraíba',
                'PE': 'Pernambuco',
                'PI': 'Piauí',
                'PR': 'Paraná',
                'RJ': 'Rio de Janeiro',
                'RN': 'Rio Grande do Norte',
                'RO': 'Rondônia',
                'RR': 'Roraima',
                'RS': 'Rio Grande do Sul',
                'SC': 'Santa Catarina',
                'SE': 'Sergipe',
                'SP': 'São Paulo',
                'TO': 'Tocantins'
                }
        return self._sigla_uf_dict
    
    # def get_cod_mun_dict(self):
    #     try:
    #         self._cod_mun_dict
    #     except:
    #         cod_mun_file = f'../dados/IBGE/RELATORIO_DTB_BRASIL_MUNICIPIO.csv'
    #         df_mun = pd.read_csv(cod_mun_file)
    #         df_mun['Nome_Município'] = df_mun['Nome_Município'].str.lower().str.replace('-','').str.replace(' ','').str.replace('ó','o').str.replace('ê','e')
    #         df_mun['key'] = df_mun['Nome_UF'] + '_' + df_mun['Nome_Município']
    #         self._cod_mun_dict = df_mun.set_index('key')['Código Município Completo'].to_dict()
        
    #     return self._cod_mun_dict
    
   
    # def get_city_code(self,municipio_uf:str):
    #     '''
    #     retorna o código do município (7 digitos) do IBGE
    #     municipio: string com nome do município e UF. 
    #     Ex: municipio_uf = "Salvador (BA)" >>> return = "2927408" (str) '''

    #     values = municipio_uf.split('(')
    #     nome_municipio = values[0].strip().lower().replace('-','').replace(' ','').replace('ó','o').replace('ê','e')
    #     if nome_municipio == 'poxoréo':
    #         nome_municipio = 'poxoréu'

    #     UF = values[1].strip(')')
    #     nome_UF = self.get_sigla_uf_dict()[UF]
    #     # print(nome_UF,nome_Município)
    #     key = nome_UF + '_' + nome_municipio
    #     cod_mun = self.get_cod_mun_dict().get(key)

    #     if not cod_mun:
    #         print('Município não encontrado!')
    #     else:
    #         cod_mun = str(cod_mun)
            
    #     return cod_mun


    # def slugify(self, x:str,sep='-',remove_char='',upper=False):
    #     x = unidecode(x)
    #     x = x.replace(remove_char,'')
    #     x = sep.join(x.split())
    #     x = x.replace("'",'')
    #     if upper:
    #         x=x.upper()
    #     else:
    #         x = x.lower()
    #     return x

    def truncate(self, original_str:str,nfloatprec:int=6):
        pat=re.compile(r'(\d+\.\d{'+str(nfloatprec)+'})\d+')
        return pat.sub(r'\1', original_str)

    def save_metadata(self):
        
        metadata = ''
        if self._year:
            metadata += f'ano: {self._year}'

        fname = f'{self._data_dir}/metadados.txt'
        with open(fname,'w') as f:
            f.write(metadata)
    
    def get_year(self):
        
        if self._year:
            year = self._year
        else:
            year = None
            fname = f'{self._data_dir}/metadados.txt'
            if os.path.exists(fname):
                with open(fname,'r') as f:
                    lines = f.readlines()
                for line in lines:
                    if 'ano' in line:
                        year = int(line.split()[1])
        
        return year


    def set_geojson_options(self):
        geom_tol = self._kwargs.get('geom_tol')

        fmt_str = self._kwargs.get('fmt_str')
        if not fmt_str:
            fmt_str = '8g'
        
        round_option = self._kwargs.get('round_option')
        if not round_option:
            round_option = 'to_json'
        
        self._geojson_options = {
            'geom_tol': geom_tol,
            'fmt_str' : fmt_str,
            'round_option': round_option
        }

    def get_geojson_options(self):
        if not self._geojson_options:
            self.set_geojson_options()

        return self._geojson_options        

    def round_coordinates(self,gdf,round_option:str='to_json',fmt_str:str='7g'):

        if round_option == 'to_json':
            out_str = gdf.to_json(drop_id=True)
            out_str = out_str.encode().decode('unicode-escape')
            split_word = '{"type": "Feature"'
            sep = '\n'
            out_str = out_str.replace(split_word,sep+split_word)
            json_lines = out_str.split(sep)
        
        elif round_option == 'temp_file':
            temp_path = f'{self._output_folder}/ibge_municipios_temp.json'
            gdf.to_file(temp_path, driver="GeoJSON")

            with open(temp_path,'r') as f:
                json_lines = f.readlines()
            os.remove(temp_path)
            sep = ''

        for index,line in enumerate(json_lines):
            i = line.find('coordinates')
            if i > 0:
                base_str = line[:i]
                coord_str = line[i:]
                coord_str = self.round_float(coord_str,fmt_str=fmt_str)
                json_lines[index] = base_str + coord_str
        out_str = sep.join(json_lines)
        return out_str

    def get_union_gd(self, fpath:str,grupo:str,verbose=True):
        gdf = gp.read_file(fpath)
        gdf = gdf.to_crs(31983)
        
        if verbose:
            area = gdf.area.sum() / 1e9
            print(f'area inicial: {area:.6g} mil km2, dimensões: {gdf.shape}')
        gdf = gdf.drop_duplicates()
        
        if verbose:
            area = gdf.area.sum() / 1e9
            print(f'area sem duplicados: {area:.6g} mil km2, dimensões: {gdf.shape}')

        gdf = gdf[['geometry']].dissolve()
        
        if verbose:
            area = gdf.area.sum() / 1e9
            print(f'area união: {area:.6g} mil km2')

        gdf['grupo'] = grupo
        return gdf

    # def ():
    #     msgs = []
    #     for i in range(len(gd_q)):
    #         print(i)
    #         for j in range(i+1,len(gd_q)):
    #             df_inter = gp.overlay(gd_q.iloc[[i]],gd_q.iloc[[j]],keep_geom_type=True)
    #             if df_inter.shape[0]:
    #                 area_inter = df_inter.area.iat[0]
    #                 area_i = gd_q.iloc[[i]].area.iat[0]
    #                 area_j = gd_q.iloc[[j]].area.iat[0]
    #                 inter_i = area_inter / area_i
    #                 inter_j = area_inter / area_j
    #                 if  (f'{inter_i:.2f}' == '1.00') and (f'{inter_j:.2f}' == '1.00'):
    #                     msgs.append(f'os poligonos {i}, {j} são coincidentes')
    #                 else:                
    #                     msgs.append(f'os poligonos {i}, {j} possuem uma área de interseção. Percentual interseção: {inter_i:.1%} de i, {inter_j:.1%} de j')
    #     out_str = '\n'.join(msgs)

    #     fname = 'log.txt'
    #     with open(fname,'w') as f:
    #         f.write(out_str)
            # i_list = [112,261]
            # i_list = [41, 115,129,398]
            # i_list = [117, 121]
            # i_list = [80, 186]
            # gd_q.iloc[i_list]