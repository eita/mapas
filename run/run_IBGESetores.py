import sys
sys.path.append('../src/')

from IBGESetores import IBGESetores

kwargs = {'geom_tol': 0.0001,'fmt_str':'7g','round_option':'to_json'}
db = IBGESetores(year=2020,**kwargs)

update = True
update = False
db.extract(update=update)

update = True
db.transform(update=update)