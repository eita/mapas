import pandas as pd
import geopandas as gp
import os

from Mapas import Mapas


class Snirh(Mapas):

    def get_name(self):
        return 'SNIRH'

    def get_input_files(self):
        ''' Método retorna uma lista vazia para a classe Snirh.'''
        return []
        
    def get_output_files(self):
        return [
            'snirh_regioes_hidrograficas.csv','municipios_regioes_hidrograficas.csv',
            'snirh_sub_bacias_nivel1.csv','municipios_sub_bacias_nivel1.csv',
            'snirh_sub_bacias_nivel2.csv','municipios_sub_bacias_nivel2.csv',
            'snirh_uepgrh.csv','municipios_uepgrh.csv', # UEPGRH Unidades Estaduais de Planejamento e Gestão de Recursos Hídricos ou unidades hidricas estaduais
            'municipios_cursos_dagua.csv'
            ]

        # return ['snirh_municipios_sub_bacias.csv','snirh_municipios_rios.csv']
        # return ['snirh_municipios_sub_bacias.csv','snirh_municipios_rios.csv']
           

    def aditional_setup(self):
        pass
    
    def setup_extract(self): 

        update = True

        input_folder = self._input_folder


        # não usado:
        # https://metadados.snirh.gov.br/files/4fd91f0d-f34f-4fca-a961-c2dcb3e0446e/geoft_bho_2017_50k_curso_dagua.gpkg

        urls = [
            'https://geoftp.ibge.gov.br/organizacao_do_territorio/malhas_territoriais/malhas_municipais/municipio_2020/Brasil/BR/BR_Municipios_2020.zip',
            'https://metadados.snirh.gov.br/geonetwork/srv/api/records/0574947a-2c5b-48d2-96a4-b07c4702bbab/attachments/SNIRH_RegioesHidrograficas_2020.zip',
            'https://metadados.snirh.gov.br/geonetwork/srv/api/records/f50527b9-24ed-41d5-b063-b5acfb25e10d/attachments/GEOFT_PNRH_SUB1.zip',
            'https://metadados.snirh.gov.br/geonetwork/srv/api/records/6141f37f-f15d-42e7-8495-ae9ddad0846f/attachments/GEOFT_PNRH_SUB2.zip',       
            'https://metadados.snirh.gov.br/geonetwork/srv/api/records/b0dd7cfd-5c47-410d-bae9-3df6e9a6dfeb/attachments/GEOFT_UNIDADE_HIDRO_ESTADUAL_2014.zip',
            'https://metadados.snirh.gov.br/files/4fd91f0d-f34f-4fca-a961-c2dcb3e0446e/geoft_bho_2017_50k_trecho_drenagem.gpkg',
            'https://metadados.snirh.gov.br/files/4fd91f0d-f34f-4fca-a961-c2dcb3e0446e/geoft_bho_2017_50k_area_drenagem.gpkg'
            ]

        for url in urls:
            fname = url.split('/')[-1] # ex: f'GEOFT_PNRH_SUB1.zip'
        
            file_path = f'{input_folder}/{fname}'
            self.url2zip(url, file_path)
            if fname.endswith('.zip'):
                self.extractFromZip(file_path)
        
        return


    def write_geojson(self, gdf, output_folder:str):

        geojson_options = self.get_geojson_options()

        geom_tol = geojson_options.get('geom_tol')
        round_option = geojson_options.get('round_option')
        fmt_str = geojson_options.get('fmt_str')

        if geom_tol:
            gdf['geometry'] = gdf['geometry'].simplify(tolerance=geom_tol)
        
        os.makedirs(output_folder,exist_ok=True)
        for _id,gd_id in gdf.groupby('id'):
            out_str = self.round_coordinates(gd_id,round_option,fmt_str)
            fname = f'{output_folder}/{_id}.json'
            with open(fname,'w') as f:
                f.write(out_str)
    
    
    def get_inter_df(self, gdf_mun, gdf, id_col:str):

        gdf = gdf.rename(columns={'id':id_col})
        gdf = gdf.to_crs(31983)
        gdf_inter = gp.overlay(gdf_mun,gdf,keep_geom_type=True)
        # gdf_inter = gdf_inter.to_crs(31983) # SIRGAS 2000 , projecao, metros
        gdf_inter['area_inter'] = gdf_inter.geometry.area
        gdf_inter['percentual_area_mun'] = gdf_inter['area_inter'] / gdf_inter['area_mun_calc']
        print('fim overlay')
        select_cols = ['CODIGO_MUNICIPIO_COMPLETO',id_col,'percentual_area_mun']
        df = gdf_inter[select_cols]
        df = df.sort_values(by='CODIGO_MUNICIPIO_COMPLETO')
        return df
        
    # def write_output_data(
    def get_gdf( #shape_file, currentet_crs, selected_columns, dtype)
        self,
        shape_file:str,
        selected_columns:list=None,
        dtype:dict={},
        currentet_crs:str=None,
        sort_columns:list=None
        ):
        
        fname = f'{self._input_folder}/{shape_file}/{shape_file}.shp'
        gdf = gp.read_file(fname)
        if selected_columns:
            selected_columns += ['geometry']
            gdf = gdf[selected_columns]
        if dtype:    
            gdf = gdf.astype(dtype)

        if not gdf.crs:
            assert_msg = f"""
    Os dados geográficos do arquivo {shape_file}.shp não estão com o sistema de coordenada definido.
    É necessário indicar esta informação no parâmetro <currentet_crs>. Ex: currentet_crs = 'EPSG:4674'.
    """
            assert currentet_crs, assert_msg
            gdf.crs = currentet_crs

        if sort_columns:
            gdf = gdf.sort_values(by=sort_columns)

        if 'id' not in gdf.columns:
            gdf.insert(0,'id',range(1,len(gdf)+1))
        else:
            gdf['id'] = range(1,len(gdf)+1)

        return gdf

    def setup_transform(self):

        csv_files = self._output_paths
        self._output_dfs = []

        shape_file = 'BR_Municipios_2020'        
        print(f'Lendo dados de {shape_file}...')
        fname = f'{self._input_folder}/{shape_file}/{shape_file}.shp'
        gdf_mun = gp.read_file(fname)
        print(gdf_mun.shape)
        print('iniciando transformação de coordenadas')
        gdf_mun = gdf_mun.to_crs(31983)
        gdf_mun['area_mun_calc'] = gdf_mun.area
        gdf_mun = gdf_mun.rename(columns={'CD_MUN':'CODIGO_MUNICIPIO_COMPLETO'})

        
        print('gdf_mun.columns:',gdf_mun.columns.to_list())
        
        ######################################################################
        shape_file = 'SNIRH_RegioesHidrograficas_2020'
        output_folder = f'{self._output_folder}/regioes_hidrograficas'
        currentet_crs = 'EPSG:4674'
        dtype = {'rhi_cd':int}
        selected_columns = ['rhi_sg','rhi_cd','rhi_nm','rhi_ar_km2']
        id_col = 'id_rhi'
        sort_columns = ['rhi_cd']
        
        print(f'Lendo dados de {shape_file}...')
        
        gdf = self.get_gdf(shape_file, selected_columns, dtype, currentet_crs, sort_columns)
        gdf['rhi_cd'] = gdf['rhi_cd'] - 1 # para ficar com os mesmos números dos outros shapes
        
        self.write_geojson(gdf,output_folder)
        
        df = gdf.drop(columns='geometry')
        
        self._output_dfs.append(df)

        df = self.get_inter_df(gdf_mun, gdf, id_col)
        self._output_dfs.append(df)


        ######################################################################
        shape_file = 'GEOFT_PNRH_SUB1'
        output_folder = f'{self._output_folder}/sub_bacias_nivel1'
        id_col = 'id_sub1'
        currentet_crs = ''
        dtype = {'PS1_CD':int}
        selected_columns = ['PS1_CD','PS1_RHI_CD','PS1_NM']
        sort_columns = ['PS1_CD']
        
        print(f'Lendo dados de {shape_file}...')
        
        gdf = self.get_gdf(shape_file, selected_columns, dtype, currentet_crs, sort_columns)
        self.write_geojson(gdf,output_folder)
        
        df = gdf.drop(columns='geometry')
        self._output_dfs.append(df)

        df = self.get_inter_df(gdf_mun, gdf, id_col)
        self._output_dfs.append(df)
        
        ######################################################################
        shape_file = 'GEOFT_PNRH_SUB2'
        output_folder = f'{self._output_folder}/sub_bacias_nivel2'
        id_col = 'id_sub2'
        currentet_crs = ''
        dtype = {'PS2_CD':int}
        selected_columns = ['PS2_CD','PS2_PS1_CD','PS2_RHI_CD','PS2_NM']
        sort_columns = ['PS2_PS1_CD','PS2_CD']
        
        print(f'Lendo dados de {shape_file}...')
        
        gdf = self.get_gdf(shape_file, selected_columns, dtype, currentet_crs, sort_columns)
        self.write_geojson(gdf,output_folder)
        
        df = gdf.drop(columns='geometry')
        self._output_dfs.append(df)

        df = self.get_inter_df(gdf_mun, gdf, id_col)
        self._output_dfs.append(df)

        ######################################################################
        shape_file = 'GEOFT_UNIDADE_HIDRO_ESTADUAL_2014'
        output_folder = f'{self._output_folder}/uepgrh'
        id_col = 'id_uepgrh'
        currentet_crs = ''
        dtype = {}
        selected_columns = ['UHE_UFD_CD','UHE_NM','UHE_AN_REF','UHE_FONTE','UHE_NM_RHI']
        sort_columns = ['UHE_UFD_CD','UHE_NM']
        
        print(f'Lendo dados de {shape_file}...')
        
        gdf = self.get_gdf(shape_file, selected_columns, dtype, currentet_crs, sort_columns)
        self.write_geojson(gdf,output_folder)
        
        df = gdf.drop(columns='geometry')
        self._output_dfs.append(df)

        df = self.get_inter_df(gdf_mun, gdf, id_col)
        self._output_dfs.append(df)
        
        ######################################################################
        #### set df_inter_rios

        # get trecho_rio_dict
        fname = 'geoft_bho_2017_50k_trecho_drenagem.gpkg'
        fpath = f'{self._input_folder}/{fname}'
        df_temp = gp.read_file(fpath)        
        df_temp = df_temp.drop('geometry',axis=1)
        df_temp = df_temp.loc[df_temp.noriocomp.notna()]
        index_col = 'cotrecho'
        values_col = 'noriocomp'
        trecho_rio_dict = df_temp.set_index(index_col)[values_col].to_dict()

        # get df_area_dren
        fname = 'geoft_bho_2017_50k_area_drenagem.gpkg'
        fpath = f'{self._input_folder}/{fname}'
        df_area_dren = gp.read_file(fpath)
        df_area_dren = df_area_dren.loc[df_area_dren.cocursodag.notna()]

        df_area_dren['NOME_RIO'] = df_area_dren[index_col].apply(lambda x: trecho_rio_dict.get(x))
        df_area_dren = df_area_dren.loc[df_area_dren['NOME_RIO'].notna()]
        df_area_dren = df_area_dren.to_crs(31983) # SIRGAS 2000 , projecao, metros

        # get df_inter_rios
        df_inter_rios = gp.overlay(gdf_mun,df_area_dren,keep_geom_type=True)
        # df_inter_rios = df_inter_rios.to_crs(31983) 
        df_inter_rios['area_inter'] = df_inter_rios.geometry.area
        df_inter_rios['percentual_area_mun'] = df_inter_rios['area_inter'] / df_inter_rios['area_mun_calc']

        selected_cols=['CODIGO_MUNICIPIO_COMPLETO','NOME_RIO','percentual_area_mun']
        df_inter_rios = df_inter_rios[selected_cols]
        # df_inter_rios = df_inter_rios.drop(columns='geometry')

        # df_inter_rios = df_inter_rios.groupby(['CODIGO_MUNICIPIO_COMPLETO','NM_MUN','AREA_KM2','noriocomp'],dropna=True)[['area_inter']].sum().reset_index()
        df_inter_rios = df_inter_rios.groupby(['CODIGO_MUNICIPIO_COMPLETO','NOME_RIO'],dropna=True)[['percentual_area_mun']].sum().reset_index()
        sort_cols = ['CODIGO_MUNICIPIO_COMPLETO','percentual_area_mun']
        df_inter_rios = df_inter_rios.sort_values(sort_cols,ascending = [1,0])
        n_rios = 3
        df_inter_rios = df_inter_rios.groupby(['CODIGO_MUNICIPIO_COMPLETO']).head(n_rios)

        # df_inter_rios = df_inter_rios.rename(columns={
        #     # 'AREA_KM2':'AREA_MUN_KM2',
        #     'noriocomp':'NOME_RIO'
        #     })
        
        self._output_dfs.append(df_inter_rios)
        

        # print(len(self._output_dfs), df_inter_rios.shape,df_inter_rios.columns.to_list())
        return


    def get_sql_column(self, column:str):
        
        if column == 'CODIGO_MUNICIPIO_COMPLETO':
            sql_column = f" `{column}` MEDIUMINT(7) UNSIGNED NOT NULL"
        elif column[:2] == 'id':
            sql_column = f" `{column}` MEDIUMINT(7) UNSIGNED NOT NULL"

        elif column in ['rhi_cd','PS1_RHI_CD','PS2_RHI_CD','UHE_UFD_CD']:
            sql_type = 'TINYINT'
            tamanho = 2
            sql_column = f" `{column}` {sql_type}({tamanho}) UNSIGNED NOT NULL"
        
        elif column in ['PS1_CD','PS2_CD','PS2_PS1_CD','UHE_AN_REF']:
            sql_type = 'SMALLINT'
            tamanho = 4
            sql_column = f" `{column}` {sql_type}({tamanho}) UNSIGNED NOT NULL"

        elif column in ['percentual_area_mun','rhi_ar_km2']:
            sql_column = f" `{column}` FLOAT NOT NULL"
        
        elif column in ['NOME_RIO','UHE_NM','UHE_FONTE']:
            tamanho = 80
            sql_column = f" `{column}` VARCHAR({tamanho}) NOT NULL"
        elif column in ['rhi_nm','PS1_NM','PS2_NM','UHE_NM_RHI']:
            tamanho = 30
            sql_column = f" `{column}` VARCHAR({tamanho}) NOT NULL"
    
        elif column == 'rhi_sg':
            tamanho = 1
            sql_column = f" `{column}` CHAR({tamanho}) NOT NULL"

        else:
            err_msg = f"""Regra não definida para a coluna {column}."""

            raise Exception(err_msg)
                
        return sql_column
        
    
    def setup_load(self,csv_file:str,sql_table:str):
        ''' funcao temporária, deverá substituir a setup_load'''
        
        sql_commands = []
        header = pd.read_csv(csv_file,nrows=0).columns.to_list()

        ### remove tabelas existentes
        drop_table = f"DROP TABLE IF EXISTS {sql_table};"
        sql_commands.append(drop_table)

        # create_table
        sql_table_columns = [self.get_sql_column(x) for x in header]
        sql_table_columns = ',\n'.join(sql_table_columns)

        create_table = f"CREATE TABLE `{sql_table}` ({sql_table_columns}) "
        create_table += ' ENGINE=MyISAM DEFAULT CHARSET=utf8;'
        
        sql_commands.append(create_table)

        # add key
        if 'CODIGO_MUNICIPIO_COMPLETO' in header:
            key_variables = ['CODIGO_MUNICIPIO_COMPLETO']
            keys_str = ', '.join([f' ADD KEY `{x}` (`{x}`)' for x in key_variables])
            add_index = f""" ALTER TABLE `{sql_table}` {keys_str} ; """
            sql_commands.append(add_index)

        # add primary_key
        if 'id' in header:
            key_variables = ['id']
            keys_str = ', '.join([f' ADD PRIMARY KEY `{x}` (`{x}`)' for x in key_variables])
            add_pk = f""" ALTER TABLE `{sql_table}` {keys_str} ; """
            sql_commands.append(add_pk)

        # load
        sql_command = self.get_load_command(sql_table,csv_file)
        sql_commands.append(sql_command)

        return sql_commands
