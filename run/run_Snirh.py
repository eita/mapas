import sys
sys.path.append('../src/')

from Snirh import Snirh

kwargs = {'geom_tol': 0.001,'fmt_str':'7g','round_option':'to_json'}
# kwargs = {'fmt_str':'7g','round_option':'to_json'}

db = Snirh(**kwargs)
db.set_verbosity(2)


update = True
update = False
db.extract(update=update)

update = True
db.transform(update=update)

local_infile=True
db.load(local_infile)