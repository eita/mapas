import sys
sys.path.append('../src/')

from FUNAITerrasIndigenas import FUNAITerrasIndigenas

kwargs = {'geom_tol': 0.0001,'fmt_str':'7g','round_option':'to_json'}
db = FUNAITerrasIndigenas(**kwargs)

update = True
update = False
db.extract(update=update)

update = True
db.transform(update=update)

db.load(primary_keys=['terrai_codigo','id'])