
import pandas as pd
import geopandas as gp
import os, time
import requests

from Mapas import Mapas

class FUNAITerrasIndigenas(Mapas):

    def get_name(self):
        return 'FUNAI_TERRAS_INDIGENAS'

    def get_input_files(self):
        # input_file = 'funai_terras_indigenas.gpkg'
        input_file = 'ti_sirgas.json'
        return [input_file]
        
    def get_output_files(self):
        return ['funai_terras_indigenas.csv','municipios_terras_indigenas.csv']

    def aditional_setup(self):
        self._output_folder += '/terras_indigenas'
        # pass
        
    def setup_extract(self): 

        url = 'http://geoserver.funai.gov.br/geoserver/Funai/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Funai:ti_sirgas&outputFormat=application%2Fjson'
        r = requests.get(url)
        if r.status_code == 404:
            raise ValueError(f'url não encontrada: {url}')
        
        fname = self._input_paths[0]
        with open(fname, 'w') as f:
            f.write(r.text)

        return

    def setup_transform(self):

        start = time.time()

        fname = self._input_paths[0]
        gdf = gp.read_file(fname)

        print('fim da leitura:',time.time()-start)

        gdf = gdf.drop(columns='id')
        id_col = 'terrai_codigo'
        name_col = 'terrai_nome'
        assert gdf[id_col].nunique() == gdf.shape[0]
        assert gdf[id_col].isna().sum() == 0
                
        df1 = gdf.drop(columns='geometry')
        # df1 = df1.rename(columns={id_col:'id'})
        sel_cols = [id_col] + df1.columns.drop(id_col).to_list()
        df1 = df1[sel_cols]

        
        gdf = gdf.to_crs(4674) # SIRGAS 2000
        selected_cols = [id_col,name_col,'geometry']
        gdf_sel = gdf[selected_cols].copy()            
        
        geom_tol = self._kwargs.get('geom_tol')
        if not geom_tol:
            geom_tol = 0.0001

        fmt_str = self._kwargs.get('fmt_str')
        if not fmt_str:
            fmt_str = '8g'
        
        round_option = self._kwargs.get('round_option')
        if not round_option:
            round_option = 'to_json'

        gdf_sel['geometry'] = gdf_sel['geometry'].simplify(tolerance=geom_tol)

        print('fim da simplificação:',time.time()-start)

        for id,gd_id in gdf_sel.groupby(id_col):
            out_str = self.round_coordinates(gd_id,round_option,fmt_str)
            fname = f'{self._output_folder}/{id}.json'
            with open(fname,'w') as f:
                f.write(out_str)

        print('fim etapa 2:',time.time()-start)

        input_dir = '../dados/IBGE_MUNICIPIOS/input'
        fname = 'ibge_br_municipios_2021.gpkg'
        fpath = f'{input_dir}/{fname}'
        gd_mun = gp.read_file(fpath)

        
        gd_mun = gd_mun.to_crs(31983) # SIRGAS 2000 , projecao, metros
        gdf = gdf.to_crs(31983)       # SIRGAS 2000 , projecao, metros
        gdf['area_terrai_km2'] = gdf.area / 1e6

        gd_inter = gp.overlay(gd_mun,gdf,keep_geom_type=True)
        gd_inter['area_inter_mun_km2'] = gd_inter.area / 1e6   
        self._gd_inter = gd_inter     
        print('fim etapa 3:',time.time()-start)

        selected_cols = ['CD_MUN','NM_MUN','SIGLA','AREA_KM2',id_col,name_col,'area_terrai_km2','area_inter_mun_km2']
        df2 = gd_inter[selected_cols].copy()
        df2 = df2.rename(columns={
            'CD_MUN':'cd_mun',
            'NM_MUN':'nm_mun',
            'SIGLA':'sigla_uf',
            'AREA_KM2':'area_mun_km2'
        })
        df2['cd_mun'] = df2['cd_mun'].astype(int)

        id_start = 1
        df2.insert(0, 'id', range(id_start,id_start+len(df2)))

        self._output_dfs = [df1,df2]
        
        return