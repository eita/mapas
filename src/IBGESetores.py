
import pandas as pd
import geopandas as gp
import os, time

from Mapas import Mapas

class IBGESetores(Mapas):

    def get_name(self):
        return 'IBGE_SETORES'

    def get_input_files(self):
        ''' Método retorna uma lista vazia para esta classe.'''
        shape_file = f'BR_Setores_{self._year}'
        input_file = f'{shape_file}/{shape_file}.shp'
        return [input_file]
        
    def get_output_files(self):
        return []   

    def aditional_setup(self):
        self._output_folder += '/ibge_setores'
        
    def setup_extract(self): 

        year = self.get_year()
        if year not in [2020]:
            raise Exception(f'Método válido apenas para o ano de 2020. Ano solicitado: {year}')
        
        url = f'https://geoftp.ibge.gov.br/organizacao_do_territorio/malhas_territoriais/malhas_de_setores_censitarios__divisoes_intramunicipais/{year}/Malha_de_setores_(shp)_Brasil/BR_Setores_{year}.zip'
        # url = f'https://geoftp.ibge.gov.br/organizacao_do_territorio/malhas_territoriais/malhas_municipais/municipio_{year}/Brasil/BR/BR_Municipios_{year}.zip'
        zip_file = url.split('/')[-1] # ex: f'BR_Municipios_2020.zip'
    
        file_path = f'{self._input_folder}/{zip_file}'
        self.url2zip(url, file_path)
        self.extractFromZip(file_path)
        return

    def setup_transform(self):

        start = time.time()

        fname = self._input_paths[0]
        gdf = gp.read_file(fname)
        # gdf = gp.read_file(fname,rows=10000)
        # gdf = gp.read_file(fname,rows=slice(-8000,None))        

        selected_cols = ['CD_MUN','CD_SETOR','CD_SIT','NM_DIST','NM_SUBDIST','geometry']
        gdf = gdf[selected_cols]
        gdf = gdf.to_crs(4674) # SIRGAS 2000

        print('fim da leitura:',time.time()-start)
        
        geom_tol = self._kwargs.get('geom_tol')
        if not geom_tol:
            geom_tol = 0.01

        fmt_str = self._kwargs.get('fmt_str')
        if not fmt_str:
            fmt_str = '7g'
        
        round_option = self._kwargs.get('round_option')
        if not round_option:
            round_option = 'to_json'

        gdf['geometry'] = gdf['geometry'].simplify(tolerance=geom_tol)

        print('fim da simplificação:',time.time()-start)    

        # os.makedirs(f'{self._output_folder}/{geom_tol}_{fmt_str}',exist_ok=True)
        
        gdf = gdf.set_index('CD_MUN')
        for cd_mun in gdf.index.unique():
            gd_mun = gdf.loc[[cd_mun]].copy()
            out_str = self.round_coordinates(gd_mun,round_option,fmt_str)
            cd_mun = str(cd_mun)[:6]
            fname = f'{self._output_folder}/{cd_mun}.json'
            # fname = f'{self._output_folder}/{geom_tol}_{fmt_str}/{cd_mun}.json'
            with open(fname,'w') as f:
                f.write(out_str)

        return
    



        