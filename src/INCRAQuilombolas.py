
import pandas as pd
import geopandas as gp
import os, time

from Mapas import Mapas

class INCRAQuilombolas(Mapas):

    def get_name(self):
        return 'INCRA_QUILOMBOLAS'

    def get_input_files(self):
        input_file = 'incra_quilombolas.gpkg'
        return [input_file]
        
    def get_output_files(self):
        return ['incra_areas_quilombolas.csv','municipios_areas_quilombolas.csv']

    def aditional_setup(self):
        self._output_folder += '/areas_quilombolas'
        # pass
        
    def setup_extract(self): 

        # year = self.get_year()
        # if year not in [2020]:
        #     raise Exception(f'Método válido apenas para o ano de 2020. Ano solicitado: {year}')
        
        # url = f'https://geoftp.ibge.gov.br/organizacao_do_territorio/malhas_territoriais/malhas_de_setores_censitarios__divisoes_intramunicipais/{year}/Malha_de_setores_(shp)_Brasil/BR_Setores_{year}.zip'
        # # url = f'https://geoftp.ibge.gov.br/organizacao_do_territorio/malhas_territoriais/malhas_municipais/municipio_{year}/Brasil/BR/BR_Municipios_{year}.zip'
        # zip_file = url.split('/')[-1] # ex: f'BR_Municipios_2020.zip'
    
        # file_path = f'{self._input_folder}/{zip_file}'
        # self.url2zip(url, file_path)
        # self.extractFromZip(file_path)
        return

    def setup_transform(self):

        start = time.time()

        fname = self._input_paths[0]
        gdf = gp.read_file(fname)

        gdf = gdf.drop_duplicates(ignore_index=True)
        gdf['id_area_quilombola'] = gdf.index + 1
                
        df1 = gdf.drop(columns='geometry')
        df1 = df1.rename(columns={'id_area_quilombola':'id'})
        sel_cols = ['id'] + df1.columns.drop('id').to_list()
        df1 = df1[sel_cols]

        
        gdf = gdf.to_crs(4674) # SIRGAS 2000
        selected_cols = ['id_area_quilombola','nm_comunid','geometry']
        gdf_sel = gdf[selected_cols].copy()        

        # print('fim da leitura:',time.time()-start)
        
        geom_tol = self._kwargs.get('geom_tol')
        if not geom_tol:
            geom_tol = 0.0001

        fmt_str = self._kwargs.get('fmt_str')
        if not fmt_str:
            fmt_str = '8g'
        
        round_option = self._kwargs.get('round_option')
        if not round_option:
            round_option = 'to_json'

        gdf_sel['geometry'] = gdf_sel['geometry'].simplify(tolerance=geom_tol)

        # print('fim da simplificação:',time.time()-start)

        for id,gd_id in gdf_sel.groupby('id_area_quilombola'):
            out_str = self.round_coordinates(gd_id,round_option,fmt_str)
            fname = f'{self._output_folder}/{id}.json'
            with open(fname,'w') as f:
                f.write(out_str)

        input_dir = '../dados/IBGE_MUNICIPIOS/input'
        fname = 'ibge_br_municipios_2021.gpkg'
        fpath = f'{input_dir}/{fname}'
        gd_mun = gp.read_file(fpath)

        
        gd_mun = gd_mun.to_crs(31983) # SIRGAS 2000 , projecao, metros
        gdf = gdf.to_crs(31983)       # SIRGAS 2000 , projecao, metros
        gdf['area_quilombola_km2'] = gdf.area / 1e6

        gd_inter = gp.overlay(gd_mun,gdf,keep_geom_type=True)
        gd_inter['area_inter_mun_km2'] = gd_inter.area / 1e6   
        self._gd_inter = gd_inter     
        
        # df = gd_inter.drop(columns='geometry')
        selected_cols = ['CD_MUN','NM_MUN','SIGLA','AREA_KM2','id_area_quilombola','nm_comunid','area_quilombola_km2','area_inter_mun_km2']
        # selected_cols = ['CD_MUN','id_area_quilombola','area_quilombola_km2','area_inter_mun_km2']
        df2 = gd_inter[selected_cols].copy()
        df2 = df2.rename(columns={
            'CD_MUN':'cd_mun',
            'NM_MUN':'nm_mun',
            'SIGLA':'sigla_uf',
            'AREA_KM2':'area_mun_km2'
        })
        df2['cd_mun'] = df2['cd_mun'].astype(int)

        id_start = 1
        df2.insert(0, 'id', range(id_start,id_start+len(df2)))

        self._output_dfs = [df1,df2]
        
        return