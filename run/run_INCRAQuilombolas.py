import sys
sys.path.append('../src/')

from INCRAQuilombolas import INCRAQuilombolas

kwargs = {'geom_tol': 0.0001,'fmt_str':'7g','round_option':'to_json'}
db = INCRAQuilombolas(**kwargs)

update = True
update = False
db.extract(update=update)

update = True
db.transform(update=update)

db.load(primary_keys=['id','id'])